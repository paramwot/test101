* TO check all the list elements are unique:

Compare length of List Input with Input set; 
By following above stage, converting List to set will remove duplicates. Thus the length of list and set will be UN-EQUAL. 


* To run this file:

#### Step-1: Enter Your Test case in "test_case.txt" file 
(e.g: [1,2,3,4]@True
[1,2,5,7]@True
)

#### Step-2: run "uniqueElementList_test.py" 

#### Step-3: check "Correct"/"Incorrect" output in "output.txt" file