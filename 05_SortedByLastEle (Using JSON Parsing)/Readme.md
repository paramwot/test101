### Exercise:
#### Problem Statement: Write a Python program to get a list, sorted in increasing order by the last element in each tuple from a given list of non-empty tuples.

#### Example: Input => [(2, 5), (1, 2), (4, 4), (2, 3), (2, 1)] Output =>[(2, 1), (1, 2), (2, 3), (4, 4), (2, 5)]

* Logic: Python "sorted" function takes diff arguments, key is one of them. We can order the sorting execution buy defining key. Here I have used lambda to set key as tuple's last element.

#### To run this example:
* Step-1: Enter Your Test case in "test_case.txt" file  ("MAINTAIN PROPER SPACING AFTER COMMA!")
* Step-2: run "sortedByLastEle_test.py" 
* Step-3: check "Correct"/"Incorrect" output in "output.txt" file