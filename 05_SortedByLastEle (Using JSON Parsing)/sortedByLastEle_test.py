from sortedByLastEle_run import sortTuple
f = open("test_cases.txt","r").read().split("\n")
new_f, case_ans, user_ans = [],[], []
for i in f:
    new_f.append(i.split("@")[0])
    case_ans.append(i.split("@")[1])
class_obj=sortTuple()
for line in new_f:
    line = list(map(str,line.replace("[", "").replace("]", "").split(","))) 
    line = tuple(line)
    line = str(line).replace("'","").replace("'","")
    print(line,"Line")
    line = list(eval(line)) 
    user_ans.append(class_obj.sort_by_lats(line))
print(case_ans)
print(user_ans)
output_file = open("output.txt","a")
output_file.truncate(0)
for i in range(len(case_ans)):
    if case_ans[i].strip(" ") == str(user_ans[i]):
        print("Correct")
        output_file.write("Correct\n")
    else:
        print("No Correct")
        output_file.write(" Not Correct\n")
output_file.close()