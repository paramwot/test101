import json
from sortedByLastEle_run import sortTuple
file = open("test_cases.json",'r').read()
json_file = json.loads(file)
new_json_file = []
output_file = open("output.txt","a")
output_file.truncate(0)
class_obj=sortTuple()
for i in json_file['case']:
    if class_obj.sort_by_lats(list(map(tuple,i['input']))) == list(map(tuple,i['output'])):
        print("Correct")
        output_file.write("Correct\n")
    else:
        print("No Correct")
        output_file.write(" Not Correct\n")
output_file.close()
# Property Decorators - Getters, Setters, and Deleters
class Student:
    num_of_student = 0
    class_name = 'Tk-1'
    def __init__(self,first,last,roll):
        self.first = first
        self.last = last
        self.roll = roll

        Student.num_of_student += 1
    @property
    def email(self):
        return '{}.{}@company.com'.format(self.first,self.last)
    @property
    def full_name(self):
        return '{} {}'.format(self.first,self.last)

    @full_name.setter
    def full_name(self,name):
        first, last = name.split(" ")
        self.first = first
        self.last = last

    @full_name.deleter
    def full_name(self):
        print("Deleted!")
        self.first = None
        self.last = None
student1 = Student("Param","Teraiya",48)
student2 = Student("Yash","vai",52)
student1.first = 'Duggo'
# print(student1.email)
# print(student2.full_name)
student2.full_name = 'Harshal Faldu'
# print(student2.first)
# print(student2.last)
# print(student2.email)
del student2.full_name